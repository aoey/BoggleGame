/* --------------------------------------------------------
 * Boggle.cpp
 * Welcome to the game of Boggle, where you play against the clock
 * to see how many words you can find using adjacent letters on the
 * board.  Each letter can be used only once for a given word.
 *
 * Author: Alexander Oey
 */
#include <iostream>
#include <fstream>
#include <cctype>
#include <ctime>
#include <string>
#include <vector>
#include <cassert>
#include <algorithm>

using namespace std;

//Global constants
const string DictionaryFileName = "dictionary.txt";
const int MaxNumberOfWords = 263533; // Number of dictionary words
const int MinWordLength = 3;         // Minimum dictionary word size to be stored
const int MaxWordLength = 16;	     // Max word size.  Add 1 for null
//const int MaxUserInputLength = 81;   // Max user input length
const int NumberOfLetters = 26;      // Letters in the alphabet
const int TotalSecondsToPlay = 60;   // Total number of seconds to play per board

/* Classes */
//Node class declaration and defintion.
class Node {
	private: //Internal data member.
		const int position; //Immutable.
		const char letter; //Immutable.
		vector<Node*> neighbourPositions; //Pointers to the adjacent nodes in the board.
		
	public: //Class public functions
		//Constructors.
		Node(int newPosition, char newLetter) : position(newPosition), letter(newLetter) { }
		~Node() { }
		
		//Getters
		Node* getNode() { //Get pointer to this Node object.
			return this;
		}
		int getPosition() const { //Get position to the Node object.
			return position;
		}
		char getLetter() const { //Get letter represented by the Node object.
			return letter;
		}
		vector<Node*> getNeighbours() const { //Get the vector that stores the pointers.
			return neighbourPositions;
		}
		
		
		//Connect Node with neighbours.
		void connect(Node* neighbour) {
			//Check if neighbour object is already inside the vector.
			if (find(neighbourPositions.begin(), neighbourPositions.end(), neighbour) != neighbourPositions.end()) {
				return; //Do nothing.
			}
			//If not, add to the vector.
			neighbourPositions.push_back(neighbour);
		}
};


/* Global functions */
//Connects nodes based on a 2x2 sub region of the board.
//a b
//c d
void connectSubRegion(const vector<Node*> &subregion) {
	//For each all elements in subregion.
	for (Node* node: subregion) {
		for (int i = 0; i < subregion.size(); ++i) {
			//Avoid connecting node with itself.
			if (node == subregion.at(i)) { //Same pointers.
				continue;
			}
			else { //Different pointers.
				node->connect(subregion.at(i)); 
			}
		}
	}
}


//Connect all nodes on the board.
void connectAll(const vector<Node*> &nodes) {
	vector<Node*> subregion;
	
	//Nine different 2x2 subregion in a 4x4 board.
	for (int row = 0; row < 3; ++row) {
		for (int col= 0; col < 3; ++col) {
			//Add all 4 node to subregion temporarily.
			subregion.push_back(nodes.at(row * 4 + col)); 
			subregion.push_back(nodes.at(row * 4 + col + 1));
			subregion.push_back(nodes.at((row + 1) * 4 + col));
			subregion.push_back(nodes.at((row + 1) * 4 + col + 1));
			connectSubRegion(subregion);
			subregion.clear();
		}
	}
}


//Display name and program information
void displayIDInfo() {
	cout << endl
		 << "Author: Alexander Oey" << endl
		 << "Program: #5, Boggle" << endl
		 << "TA: Minh Huynh Nguyen" << endl
		 << "Lab: Tues 11am" << endl << endl;
}


//Display instructions.
void displayInstructions() {
	printf("Welcome to the game of Boggle, where you play against the clock   \n");
    printf("to see how many words you can find using adjacent letters on the  \n");
    printf("board.  Each letter can be used only once for a given word.       \n");
    printf("  \n");
    printf("When prompted to provide input you may also:                      \n");
    printf("     Enter 'r' to reset the board to user-defined values.         \n");
    printf("     Enter 's' to solve the board and display all possible words. \n");
    printf("     Enter 't' to toggle the timer on/off.                        \n");
    printf("     Enter 'x' to exit the program.                               \n");
    printf("  \n");
}


//Print board
void printBoard(const vector<Node*> &board) {
	//Print all elements of vector board.
	for (int i = 0; i < board.size(); ++i) {
		//Print carriage return every multiple of 4.
		if (i % 4 == 0) { 
			cout << endl;
		}
		cout << board.at(i)->getLetter() << " ";
	}
	cout << endl;
}


//Helper function to sort words list before displaying to user.
bool compareString(string &first, string &second) {
	//Find appropriate comparison scheme.
	if (first.size() == second.size()) { //Same size.
		return (first < second); //Compare lexicographically.
	}
	else { //Different size.
		return (first.size() < second.size()); //Compare by size.
	}
}


//Print words found by alphabetical order and shortest first.
void printWordsFound(vector<string> &wordsFound) {
	//Sort words.
	sort(wordsFound.begin(), wordsFound.end(), compareString);
	
	//Print words.
	for (int i = 0; i < wordsFound.size(); ++i) {
		cout << wordsFound.at(i) << " ";
	}
	cout << endl;
}


//Search through the adjacent nodes by expanding it as a tree-like structure. 
bool expandSearchTree(Node* parent, string userInput, int depth, vector<Node*> &parents) {
	vector<Node*> children = parent->getNeighbours(); //List of adjacent nodes of parent.
	bool checkChildren = false; //Flag for results of search.
	
	//String index has been surpassed by depth.
	//When this is reached, every character in the word has passed test.
	//Hence, the word exist on the board.
	if (userInput.size() == depth) {
		return true;
	}
	
	//Current node does not match the string at the current depth.
	if (parent->getLetter() != userInput.at(depth)) {
		parents.pop_back(); //Remove parent from the vector because recursive call is going back.
		return false;
	}
	else { //Current node matches the string at the current depth.
		for (Node* node: children) { //Consider all the adjacent nodes of parent.
			if (find(parents.begin(), parents.end(), node) == parents.end() && !checkChildren) {
				parents.push_back(parent); //To avoid exploring the node twice.
				//checkChildren is true if at least one expansion is correct.
				checkChildren = checkChildren || expandSearchTree(node, userInput, depth+1, parents);
			}
		}
	}
	return checkChildren;
}


//Check solution.
bool verifySolution(vector<Node*> &board, string userInput) {
	Node* firstNode; //Node to the first letter of the word.
	vector<Node*> parents; //The parents of the current node.
	bool result = false; //Flag for results of the search.
	
	//Search for the first node in the board.
	for (Node* node: board) {
		//Found first node and word has not been found on the board.
		if (node->getLetter() == userInput.at(0) && !result) {
			firstNode = node;
			result = result || expandSearchTree(firstNode, userInput, 0, parents);
		}
	}
	return result;
}


// Read in dictionary
// First dynamically allocate space for the dictionary.  Then read in words
// from file.  Note that the '&' is needed so that the new array address is
// passed back as a reference parameter.
void readInDictionary(vector<string> &dictionary) {
    //Set up input stream
    ifstream fileInputStream; //Create file input stream object.
    fileInputStream.open(DictionaryFileName);
	
	//Check if file fails to open.
    assert(!fileInputStream.fail());       
    
    //Read from stream.
    string tempWord; //Store string to be pushed back to dictionary vector.
    while (fileInputStream >> tempWord) {
        if (tempWord.length() >= MinWordLength && tempWord.length() <= MaxWordLength) {
            dictionary.push_back(tempWord);
        }
    }
    
    cout << "The dictionary total number of words is: " << MaxNumberOfWords << endl;
    cout << "Number of words of the right length is:  " << dictionary.size() << endl;
    
    //Close the input stream.
    fileInputStream.close();
}


// Get random character
// Find random character using a table of letter frequency counts.
// Iterate through the array and find the first position where the random number is
// less than the value stored.  The resulting index position corresponds to the
// letter to be generated (0='a', 1='b', etc.)
char getRandomCharacter() {
    // The following table of values came from the frequency distribution of letters in the dictionary
    float letterPercentTotals[NumberOfLetters] = {
                0.07680,  //  a
                0.09485,  //  b
                0.13527,  //  c
                0.16824,  //  d
                0.28129,  //  e
                0.29299,  //  f
                0.32033,  //  g
                0.34499,  //  h
                0.43625,  //  i
                0.43783,  //  j
                0.44627,  //  k
                0.49865,  //  l
                0.52743,  //  m
                0.59567,  //  n
                0.66222,  //  o
                0.69246,  //  p
                0.69246,  //  q
                0.76380,  //  r
                0.86042,  //  s
                0.92666,  //  t
                0.95963,  //  u
                0.96892,  //  v
                0.97616,  //  w
                0.97892,  //  x
                0.99510,  //  y
                1.00000}; //  z
    
    // generate a random number between 0..1
    // Multiply by 1.0 otherwise integer division truncates remainders
    float randomNumber = 1.0 * rand() / RAND_MAX;
    
    // Find the first position where our random number is less than the
    // value stored.  The index corresponds to the letter to be returned,
    // where 'a' is 0, 'b' is 1, and so on.
    for (int i = 0; i < NumberOfLetters; i++) {
        if (randomNumber < letterPercentTotals[i]) {
            // we found the spot.  Return the corresponding letter
            return (char) 'a' + i;
        }
    }
    
    //Sanity check
    cout << "No alphabetic character generated.  This should not have happened. Exiting program.\n";
    exit(-1);
    return ' ';   // should never get this
}


//Reset the board.
void resetBoard(vector<Node*> &boardNodes) {
	//Remove all elements in the vector.
	boardNodes.clear();
	
	//Take user input and reflect on boardNodes vector.
	cout << "Enter 16 characters to be used to set the board: ";
	char resetInput;
	for (int i = 0; i < 16; ++i) {
		cin >> resetInput;
		Node* temp = new Node(i, resetInput);
		boardNodes.push_back(temp);
	}
}


//Solve the board.
void solveBoard(vector<Node*> &boardNodes, vector<string> &dictionary) {
	int min = 0; //Minimum word length.
	int max = 0; //Maximum word length.
	vector<string> solutions; //List of solutions.
	
	//Get user input.
	cout << "Enter min and max word lengths to display: " << endl;
	cin >> min >> max;
	
	//Check if word in dictionary is on board.
	for (string &word: dictionary) {
		if (word.size() >= min && word.size() <= max) {
			if (verifySolution(boardNodes, word)) {
				solutions.push_back(word);
			}
		}
	}
	
	//Print solutions.
	cout << "Words between " << min << " and " << max << " are: ";
	printWordsFound(solutions);
	cout << endl;
}


//Calculate score
int calculateScore(string userInput) {
	if (userInput.size() < 3) { //word length < 3.
		return 0;
	}
	else if (userInput.size() == 3) { //word length 3.
		return 1;
	}
	else if (userInput.size() == 4) { //word length 4.
		return 2;
	}
	else if (userInput.size() == 5) { //word length 5.
		return 3;
	}
	else { //word length > 6.
		return userInput.size();
	}
}
	
	
//Main
int main() {
	//Variable declarations.
	vector<string> dictionary; //Vector of dictionary words, dynamically allocated
	vector<Node*> boardNodes; //Stores board nodes.
	vector<string> wordsFound; //Word found by user.
    string userInput; //Stores user input.
	int counter = 1; //Move counter.
	int score = 0; //Store user's score.
	bool runTimer = true; //Flag to toggle timer.
	
	//Declare variable to hold time and get the current time.
	time_t startTime = time(NULL);
	
	//Print program information.
	displayIDInfo();
	displayInstructions();
	
	//Read in dictionary.
    readInDictionary(dictionary);
	
	//Set random seed
	srand(time(0));
    
	//Get random character for the game board.
	for (int i = 0; i < 16; ++i) {
		Node* temp = new Node(i, getRandomCharacter());
		boardNodes.push_back(temp);
	}
	
	//Connect all the adjacent nodes in the board.
	connectAll(boardNodes);
	
	//Loop game while there is still time left.
    int elapsedSeconds = 0;
	time_t timerToggleStart = 0;
	int timerDowntime = 0;
    while (elapsedSeconds < TotalSecondsToPlay) {
		//Print how many seconds user have if timer turned on.
		if(runTimer) {
			cout << endl << "   " << TotalSecondsToPlay - elapsedSeconds << " seconds remaining." << endl;
		}
		else {
			cout << endl << "   " << "Timer is OFF" << endl;
		}
		
		//Print board.
		printBoard(boardNodes);
		cout << "   Score: " << score << endl;
        //Prompt for and get user input
        cout << endl;
        cout << counter << ". Enter a word: ";
        cin >> userInput;
		
		//Change all characters in userInput to lower case.
		transform(userInput.begin(), userInput.end(), userInput.begin(), ::tolower);
		
		//Special cases for userInput.
		if (userInput == "x") { //Exit.
			break;
		}
		else if (userInput == "r") { //Reset.
			resetBoard(boardNodes);
			//Connect all the adjacent nodes in the board.
			connectAll(boardNodes);
			continue;
		}
		else if (userInput == "t") { //Toggle timer.
			if (runTimer == false) {
				timerDowntime = difftime(time(NULL), timerToggleStart);
				startTime += timerDowntime;
			}
			else {
				timerDowntime = 0;
				timerToggleStart = time(NULL);
			}
			runTimer = !runTimer;
			continue;
		}
		else if (userInput == "s") { //Solve puzzle.
			solveBoard(boardNodes, dictionary);
			break;
		}
		else {
			//Check if word has been find by user.
			if (find(wordsFound.begin(), wordsFound.end(), userInput) != wordsFound.end()) {
				cout << "Sorry that was previously found." << endl;
				cout << "Words found so far are: ";
				printWordsFound(wordsFound);
				elapsedSeconds = difftime(time(NULL), startTime); //Record elapsed time.
				continue; //Skip all other steps so counter not incremented.
			}
			
			//Check if word exist in dictionary.
			if (binary_search(dictionary.begin(), dictionary.end(), userInput)) {
				cout << userInput << " is in the dictionary." << endl;
				//Check if word is on board.
				if (verifySolution(boardNodes, userInput)) { //Word is on board.
					cout << userInput << " is in the board." << endl;
					cout << "    Worth " << calculateScore(userInput) << " points." << endl;
					score += calculateScore(userInput); //Calculate total score.
					wordsFound.push_back(userInput);
					
					//Print words found.
					cout << "Words found so far are: ";
					printWordsFound(wordsFound);
				}
				else { //Word not on board.
					cout << userInput << " is not in the board" << endl;
					--counter; //Decrement counter to balance increment later.
				}
			}
			else { //Word does not exist in dictionary.
				cout << userInput << " is NOT in the dictionary." << endl;
				--counter; //Decrement counter to balance increment later.
			}
		}
		//Change elapsedSeconds depending on whether timer is toggled or not.
		if(runTimer) {
			//Calculate how many seconds have elapsed since we started the timer.
			elapsedSeconds = difftime(time(NULL), startTime);
		}
		
		//Increment counter.
		++counter;
    }
	cout << endl;
    cout << "Exiting the program." << endl;
    return 0;  
}
	
	